namespace Model.Games.Bingo 
{
    public interface IBingoRule 
    {
        string Name { get; }
        bool Check(BingoBoard board);
    }
}