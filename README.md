# 888 Test

Hello there!

This app was developed as test task while interviewing to 888 holding.

This is a console app that uses .NET Core 2.2.
You can download it here: https://dotnet.microsoft.com/download/dotnet-core/2.2

Also you might need to generate an SSL sertificate to run this app locally.

This app was created under Mac OS.
Currently it's unclear how it will work under Windows.

Good luck! =)