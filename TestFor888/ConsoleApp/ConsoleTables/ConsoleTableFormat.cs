namespace ConsoleApp.ConsoleTables 
{
    public enum ConsoleTableFormat
    {
        Default = 0,
        MarkDown = 1,
        Alternative = 2,
        Minimal = 3
    }
}