using System;
using System.Collections.Generic;

namespace Model.Games.Bingo 
{
    public sealed class BingoSelectedIndexesRule : IBingoRule 
    {
        public string Name         { get; }
        public List<int> Indexes   { get; }
        
        public BingoSelectedIndexesRule(string name, List<int> indexes)
        {
            Name = name;
            Indexes = indexes;
        }
        
        public bool Check(BingoBoard board)
        {
            foreach (var index in Indexes) {
                var number = board.GetNumber(index);
                if (!board.CheckNumberIsSelected(number))
                    return false;    
            }
            
            Console.WriteLine(Name);
            return true;
        }
    }
}