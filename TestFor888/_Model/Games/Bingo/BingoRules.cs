using System.Collections.Generic;

namespace Model.Games.Bingo {
    public sealed class BingoRules 
    {
        public List<IBingoRule> Rules         { get; }
        public List<IBingoRule> RulesMatched  { get; private set; }
        
        public BingoRules()
        {
            Rules = new List<IBingoRule>();
            RulesMatched = new List<IBingoRule>();
        }
        
        public BingoRules AddRule(IBingoRule rule)
        {
            Rules.Add(rule);
            return this;
        }
        
        public bool CheckRulesMatching(BingoBoard board) {
            var matched = new List<IBingoRule>();
            foreach (var rule in Rules) {
                if (rule.Check(board))
                    matched.Add(rule);
            }
            RulesMatched = matched;
            return RulesMatched.Count > 0;
        }
    }
}