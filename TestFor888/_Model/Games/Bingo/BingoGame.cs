﻿using System;

namespace Model.Games.Bingo
{    
    public class BingoGame 
    {
        public BingoBoard Board         { get; private set; }
        public BingoRules Rules         { get; private set; }
        public BingoGameState State     { get; private set; }
        public bool IsWin               { get; private set; }
        
        public void Start()
        {
            Board.Fill();
            State = BingoGameState.Started;
            IsWin = false;
        }
        
        public int DrawNumber()
        {
            var random = new Random();
            var index = random.Next(Board.NumbersAvailable.Count);
            var number = Board.NumbersAvailable[index];
            Board.NumbersAvailable.RemoveAt(index);
            
            Board.DrawsAvailable--;
            if (Board.DrawsAvailable == 0)
                State = BingoGameState.Finished;
            
            return number;
        }
        
        public bool CheckWinCombo()
        {
            IsWin = Rules.CheckRulesMatching(Board);
            return IsWin;
        }
        
        public static BingoGame Create(BingoBoard board, BingoRules rules) 
        {
            if (board == null)
                throw new Exception("Board can't be null.");
            
            if (rules == null)
                throw new Exception("Rules can't be null.");
            
            var game = new BingoGame
            {
                State = BingoGameState.New, 
                Board = board,
                Rules = rules 
            };
            return game;
        }
    }
}