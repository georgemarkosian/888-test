﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApp.ConsoleTables;
using Model.Games.Bingo;

namespace ConsoleApp 
{
    public sealed class Program 
    {
        private static BingoGame Game;
        
        public static void Main(string[] args) 
        {
            Console.Clear();
            Console.WriteLine("Hello there!");
            Console.WriteLine("Welcome to The Awesome Console Bingo!");
            Console.WriteLine("Press any key to start...");
            Console.ReadKey();
            
            StartGame();

            var firstDraw = true;
            
            int number;
            bool numberMatched = false;
            string numberPrevious = null;
            
            while (Game.State == BingoGameState.Started) {
                
                if (!firstDraw) {
                    number = Game.DrawNumber();
                    numberPrevious = number.ToString();
                    
                    numberMatched = Game.Board.SelectNumber(number);
                    if (numberMatched)
                        Game.CheckWinCombo();
                } 
                else
                {
                    firstDraw = false;
                }
                
                PrintGameState(numberPrevious, numberMatched);
                
                Console.WriteLine("Press any key to draw a number...");
                Console.WriteLine();
                Console.ReadKey();
            }
            
            PrintGameState(numberPrevious, numberMatched);
            
            if (Game.IsWin)
            {
                Console.WriteLine();
                Console.WriteLine("WINNER! WINNER! CHICKEN DINNER!");
                Console.WriteLine("Congratulations!");
                Console.WriteLine("You won!");
            } else {
                Console.WriteLine();
                Console.WriteLine("Good luck next time!");
            }
            
            Console.WriteLine();
            Console.WriteLine("Thanks for stopping by!");
            Console.WriteLine("Hope to see you soon again.");
            Console.WriteLine("Press any key to exit...");
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }
        
        private static void StartGame() {
            if (Game != null)
                throw new Exception("Can't start a bingo game. Another game is running currently.");
            
            var board = new BingoBoard(5, 5, Enumerable.Range(1, 52).ToList(), 5 * 5);
            
            var rules = new BingoRules()
                .AddRule(new BingoSelectedIndexesRule("Horizontal #1", new List<int> { 0,  1,  2,  3,  4}))
                .AddRule(new BingoSelectedIndexesRule("Horizontal #2", new List<int> { 5,  6,  7,  8,  9}))
                .AddRule(new BingoSelectedIndexesRule("Horizontal #3", new List<int> {10, 11, 12, 13, 14}))
                .AddRule(new BingoSelectedIndexesRule("Horizontal #4", new List<int> {15, 16, 17, 18, 19}))
                .AddRule(new BingoSelectedIndexesRule("Horizontal #5", new List<int> {20, 21, 22, 23, 24}))
                
                .AddRule(new BingoSelectedIndexesRule("Vertical #1", new List<int> { 0,  5, 10, 15, 20}))
                .AddRule(new BingoSelectedIndexesRule("Vertical #2", new List<int> { 1,  6, 11, 16, 21}))
                .AddRule(new BingoSelectedIndexesRule("Vertical #3", new List<int> { 2,  7, 12, 17, 22}))
                .AddRule(new BingoSelectedIndexesRule("Vertical #4", new List<int> { 3,  8, 13, 18, 23}))
                .AddRule(new BingoSelectedIndexesRule("Vertical #5", new List<int> { 4,  9, 14, 19, 24}))
                
                .AddRule(new BingoSelectedIndexesRule("Diagonal #1", new List<int> {0, 6, 12, 18, 24}))
                .AddRule(new BingoSelectedIndexesRule("Diagonal #2", new List<int> {4, 8, 12, 16, 20}));
            
            var game = BingoGame.Create(board, rules);
            game.Start();
            
            Game = game;
        }
        
        private static void PrintGameState(string numberPrevious, bool numberMatched)
        {
            Console.Clear();
                
            PrintBoard();
            
            string message;
            if (numberPrevious != null) {
                message = numberMatched ? $"Number drawn: {numberPrevious} MATCH!" 
                                        : $"Number drawn: {numberPrevious}";
            } else {
                message = "Number drawn: NONE";
            }
            Console.WriteLine(message);
            
            var rules = new List<string>();
            foreach (var rule in Game.Rules.RulesMatched) {
                rules.Add(rule.Name);
            }
            Console.WriteLine($"Matched rules: {string.Join(", ", rules.ToArray())}");
            
            Console.WriteLine();
            Console.WriteLine($"Draws to go: {Game.Board.DrawsAvailable}");
        }
        
        private static void PrintBoard()
        {
            var headers = new string[Game.Board.SizeX];
            for (var i = 0; i < Game.Board.SizeX; i++)
                headers[i] = $" {i + 1} ";

            var tableOptions = new ConsoleTableOptions 
            { 
                Columns = new List<string>(headers)
            };
            
            var table = new ConsoleTable(tableOptions);
            
            for (var y = 0; y < Game.Board.SizeY; y++) {
                var row = new object[Game.Board.SizeX];
                for (var x = 0; x < Game.Board.SizeX; x++) {
                    var number = Game.Board.GetNumber(x, y);
                    var selected = Game.Board.CheckNumberIsSelected(number);
                    row[x] = selected ? $"{number}*" : number.ToString();
                }
                table.AddRow(row);
            }
            
            Console.WriteLine("Board:");
            table.Write();
        }
    }
}