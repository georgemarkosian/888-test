using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Games.Bingo
{
    public sealed class BingoBoard 
    {
        public int SizeX                   { get; }
        public int SizeY                   { get; }
               
        public List<int> NumbersRange      { get; }
        public List<int> NumbersCurrent    { get; private set; }
        public List<int> NumbersSelected   { get; private set; }
        public List<int> NumbersAvailable  { get; private set; }
        
        public int CellsCount              => SizeX * SizeY;
        public int DrawsAvailable          { get; set; }
        
        public BingoBoard(int sizeX, int sizeY, List<int> numbersRange, int drawsCount)
        {
            SizeX = sizeX;
            SizeY = sizeY;
            NumbersRange = numbersRange;
            DrawsAvailable = drawsCount;
        }
        
        public void Fill()
        {
            if (NumbersCurrent != null)
                throw new Exception("Board already filled.");
            
            var random = new Random();
            var capacity = CellsCount;
            var numbersAvailable = NumbersRange.ToList();
            var numbersCurrent = new List<int>();
            
            for (var i = 0; i < capacity; i++)
            {
                var index = random.Next(numbersAvailable.Count);
                numbersCurrent.Add(numbersAvailable[index]);
                numbersAvailable.RemoveAt(index);
            }
            
            NumbersCurrent = numbersCurrent;
            NumbersSelected = new List<int>(numbersCurrent.Count);
            NumbersAvailable = NumbersRange.ToList();
        }
        
        public int GetNumber(int index)
        {
            var number = NumbersCurrent[index];
            return number;
        }
        
        public int GetNumber(int x, int y)
        {
            var index = SizeX * y + x;
            var number = NumbersCurrent[index];
            return number;
        }

        public bool CheckNumberIsSelected(int number)
        {
            if (NumbersCurrent.Contains(number) && NumbersSelected.Contains(number))
                return true;
            return false;
        }
        
        public bool SelectNumber(int number)
        {
            if (NumbersCurrent.Contains(number) && !NumbersSelected.Contains(number)) {
                NumbersSelected.Add(number);
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            var res = String.Join(", ", NumbersCurrent.ToArray());
            return res;
        }
    }
}