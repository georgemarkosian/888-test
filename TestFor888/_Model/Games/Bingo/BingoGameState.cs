namespace Model.Games.Bingo 
{
    public enum BingoGameState 
    {
        New,
        Started,
        Finished
    }
}